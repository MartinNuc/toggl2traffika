#!/bin/bash

function printHelp {
    echo "Usage $0 [year] [month]"
    exit 1;
}

if ([ -z $1 ] || [ -z $2 ]); then
   printHelp;
fi

YEAR=$1
MONTH=$2
LAST_DAY=`date -v1d -v${MONTH}m -v+1m -v${YEAR}y -v-1d "+%d"`

for i in `seq -w 01 2 ${LAST_DAY}`; do toggl2traffika -sd ${YEAR}-${MONTH}-$i; done

